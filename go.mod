module bitbucket.org/rnix/guess-song

go 1.14

require (
	github.com/go-ini/ini v1.57.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	gopkg.in/telegram-bot-api.v4 v4.6.4
)
